package com.example.homework;

import com.example.homework.service.LoanService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.AssertionErrors;

@SpringBootTest
public class LoanServiceTest {

    @Autowired
    LoanService loanService;

    @Test
    void personInDebt() throws Exception {
        LoanService.LoanDetails loanDetails = loanService.getLoan(456, 2000, 12);
        AssertionErrors.assertEquals("loan is not granted", false, loanDetails.loanGranted());
    }

    @Test
    void loanGranted() throws Exception {
        LoanService.LoanDetails loanDetails = loanService.getLoan(123, 2000, 24);
        AssertionErrors.assertEquals("is loan 12000", 12000, loanDetails.loanAmount());
        AssertionErrors.assertEquals("is is period 24", 24, loanDetails.loanPeriod());
    }

    @Test
    void loanGrantedForLongerPeriod() throws Exception {
        LoanService.LoanDetails loanDetails = loanService.getLoan(123, 2000, 12);
        AssertionErrors.assertEquals("is loan 12000", 12000, loanDetails.loanAmount());
        AssertionErrors.assertEquals("is is period 24", 24, loanDetails.loanPeriod());
    }

    @Test
    void loanTooGreat() throws Exception {
        LoanService.LoanDetails loanDetails = loanService.getLoan(123, 300000, 12);
        AssertionErrors.assertEquals("is loan 36000", 36000, loanDetails.loanAmount());
        AssertionErrors.assertEquals("is is period 36", 36, loanDetails.loanPeriod());
    }

    @Test
    void testNonStandardPeriod() throws Exception {
        LoanService.LoanDetails loanDetails = loanService.getLoan(123, 2000, 13);
        AssertionErrors.assertEquals("is loan 12000", 12000, loanDetails.loanAmount());
        AssertionErrors.assertEquals("is is period 24", 24, loanDetails.loanPeriod());
    }
}
