package com.example.homework;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class LoanControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void noParameters() throws Exception {
        mockMvc.perform(post("/loan")).andDo(print()).andExpect(status().is4xxClientError());
    }

    @Test
    void wrongLoanAmount() throws Exception {
        mockMvc.perform(post("/loan?personCode=1&loanAmount=1000&loanPeriod=12")).andDo(print()).andExpect(status().is4xxClientError());
    }

    @Test
    void wrongLoanPeriod() throws Exception {
        mockMvc.perform(post("/loan?personCode=1&loanAmount=2000&loanPeriod=11")).andDo(print()).andExpect(status().is4xxClientError());
    }

    @Test
    void correctCall() throws Exception {
        mockMvc.perform(post("/loan?personCode=1&loanAmount=2000&loanPeriod=12")).andDo(print()).andExpect(status().isOk());
    }

}
