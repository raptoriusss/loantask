package com.example.homework.controller;

import com.example.homework.service.LoanService;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("loan")
public class LoanController {

    @Autowired
    private LoanService loanService;

    @PostMapping
    public LoanService.LoanDetails getLoan(@RequestParam Integer personCode,
                          @RequestParam @Min(2000) @Max(10000) Integer loanAmount,
                          @RequestParam @Min(12) @Max(60) Integer loanPeriod) {
        return loanService.getLoan(personCode, loanAmount, loanPeriod);
    }

}
