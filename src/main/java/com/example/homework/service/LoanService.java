package com.example.homework.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;

@Service
public class LoanService {

    @Autowired
    PersonDetailsService personDetailsService;

    public LoanDetails getLoan(Integer personCode, Integer loanAmount, Integer loanPeriod) {
        Optional<PersonDetailsService.PersonDetails> personDetails = personDetailsService.getPersonDetails(personCode.toString());

//        check if person has debt or any loan options available from service
        if (personDetails.isPresent() && !personDetails.get().debt()) {
            PersonDetailsService.PersonDetails details = personDetails.get();

//            calculate credit score for specific loan period
            float creditScore = calculateCreditScore(details.creditValueSegments().get(loanPeriod), loanAmount, loanPeriod);
            if (creditScore < 1) {
//                loan period insufficient. check for longer sufficient periods
                return details.creditValueSegments().entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue))
                        .filter(entry -> calculateCreditScore(entry.getValue(), loanAmount, loanPeriod) > 1)
                        .map(entry -> new LoanDetails(personCode, calculateMaximumLoanForPeriod(entry.getKey(), entry.getValue()), entry.getKey()))
                        //else return maximum available loan
                        .findFirst().orElse(getMaximumTotalLoan(details));
            }
//            loan may be granted for the requested time period
            return new LoanDetails(personCode, calculateMaximumLoanForPeriod(loanPeriod, details.creditValueSegments().get(loanPeriod)), loanPeriod);
        }
//        loan rejected
        return new LoanDetails(personCode);
    }

    private LoanDetails getMaximumTotalLoan(PersonDetailsService.PersonDetails personDetails) {
        return personDetails.creditValueSegments().entrySet().stream().sorted((o1, o2) -> o2.getValue().compareTo(o2.getValue()))
                .map(entry -> new LoanDetails(personDetails.personCode(), calculateMaximumLoanForPeriod(entry.getKey(), entry.getValue()), entry.getKey()))
                .findFirst().orElse(new LoanDetails(personDetails.personCode()));
    }

    private float calculateCreditScore(Integer creditModifier, Integer loanAmount, Integer loanPeriod) {
        if (creditModifier != null && creditModifier > 0) {
            return ((float) creditModifier / loanAmount) * loanPeriod;
        }
        return 0;
    }

    private Integer calculateMaximumLoanForPeriod(Integer period, Integer modifier) {
        return period * modifier;
    }

    public record LoanDetails(Integer personCode,
                              @JsonInclude(JsonInclude.Include.NON_NULL) Integer loanAmount,
                              @JsonInclude(JsonInclude.Include.NON_NULL) Integer loanPeriod,
                              Boolean loanGranted) {
        public LoanDetails(Integer personCode) {
            this(personCode, null, null, false);
        }

        public LoanDetails(Integer personCode, Integer loanAmount, Integer loanPeriod) {
            this(personCode, loanAmount, loanPeriod, true);
        }
    }

}
