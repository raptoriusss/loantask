package com.example.homework.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PersonDetailsService {

    private List<PersonDetails> detailsList;

    public PersonDetailsService() {
        PersonDetails person1 = new PersonDetails(123, new HashMap<>(), false);
        person1.creditValueSegments().put(12, 100);
        person1.creditValueSegments().put(36, 1000);
        person1.creditValueSegments().put(24, 500);
        PersonDetails person2 = new PersonDetails(234, Map.of(12, 800), false);
        PersonDetails person3 = new PersonDetails(345, Map.of(24, 5000), false);
        PersonDetails person4 = new PersonDetails(456, Map.of(36, 6000), true);
        PersonDetails person5 = new PersonDetails(567, Map.of(60, 100000), false);
        detailsList = List.of(person1, person2, person3, person4, person5);
    }

    public Optional<PersonDetails> getPersonDetails(String personCode) {
        return detailsList.stream()
                .filter(personDetails -> personDetails.personCode().equals(Integer.parseInt(personCode)))
                .findFirst();
    }

    public record PersonDetails(Integer personCode, Map<Integer, Integer> creditValueSegments, Boolean debt) {
    }
}
